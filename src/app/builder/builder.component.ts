import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-builder',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.css']
})
export class BuilderComponent implements OnInit {
  @ViewChild('json', {static: true}) jsonElement?: ElementRef;
  public form: {};
  constructor() {
    this.form = {components: []};
  }

  ngOnInit(): void {
  }

  onChange(event){
    this.jsonElement.nativeElement.innerHTML = '';
    this.jsonElement.nativeElement.appendChild(document.createTextNode(JSON.stringify(event.form, null, 4)));
  }
}
